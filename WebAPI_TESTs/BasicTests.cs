using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Net;
using System.Net.Http;
using WebAPI.Controllers;
using Xunit;

namespace WebAPI_TESTs
{
    public class BasicTests
    {
        [Fact]
        public void Check_Empty_storage()
        {
            const int ID = 1;

            MainController controller = new MainController
            {
                Request = new HttpRequestMessage()
            };
            var result = controller.Get(ID);
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async void Check_Created()
        {
            Customer customer = new Customer()
            {
                Id = 1,
                Email = "fakemail@mail.com",
                FullName = "FullName",
                Phone = "Phone: 7776221"
            };
            MainController controller = new MainController
            {
                Request = new HttpRequestMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(customer))
                }
            };
            var result = await controller.Post();
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
        }

        [Fact]
        public async void Check_Duplicate_Creation()
        {
            Customer customer = new Customer()
            {
                Id = 1,
                Email = "fakemail@mail.com",
                FullName = "FullName",
                Phone = "Phone: 7776221"
            };
            MainController controller = new MainController
            {
                Request = new HttpRequestMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(customer))
                }
            };
            await controller.Post();
            var result = await controller.Post();
            Assert.Equal(HttpStatusCode.Conflict, result.StatusCode);
        }

        [Fact]
        public async void Check_Create_Receive()
        {
            const int ID = 1;

            Customer customer = new Customer()
            {
                Id = ID,
                Email = "fakemail@mail.com",
                FullName = "FullName",
                Phone = "Phone: 7776221"
            };
            var jsonContent = JsonConvert.SerializeObject(customer);
            MainController controller = new MainController
            {
                Request = new HttpRequestMessage()
                {
                    Content = new StringContent(jsonContent)
                }
            };
            var result = await controller.Post();
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new System.Web.Http.HttpConfiguration());
            result = controller.Get(ID);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal(jsonContent, (await result.Content.ReadAsStringAsync()).Replace("\\","").Substring(1, jsonContent.Length));
        }
    }
}
