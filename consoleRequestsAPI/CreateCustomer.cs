﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Net.Http;
using System.Threading.Tasks;

namespace consoleRequestsAPI
{
    public static class CreateCustomer
    {
        private static HttpClient Client = new HttpClient();
        public static async Task<string> CreateApiCustomer(Customer customer)
        {
            try
            {
                return (await Client.PostAsync("http://localhost:55932/customer", new StringContent(JsonConvert.SerializeObject(customer)))).StatusCode.ToString();
            }
            catch
            {
                return "Check if API running";
            }
        }
    }
}
