﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consoleRequestsAPI
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Введите 1 - чтобы перейти к запросу пользователя по ID или введите 5 для создания случайного пользователя");
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.D1 || key.Key == ConsoleKey.NumPad1)
                {
                    Console.WriteLine("\nВведите ID пользователя: ");
                    var fromConsole = Console.ReadLine();
                    int id = Convert.ToInt32(fromConsole);
                    Console.WriteLine("\nGet customer: " + await GetCustomer.GetApiCustomer(id));
                }
                else if (key.Key == ConsoleKey.D5 || key.Key == ConsoleKey.NumPad5)
                {
                    int id = Convert.ToInt32(DateTime.UtcNow.TimeOfDay.TotalMilliseconds % 100000);
                    Console.WriteLine("Создаем пользователя с ID " + id);
                    Customer customer = new Customer()
                    {
                        Id = id,
                        Email = "fakemail@mail.com",
                        FullName = "FullName",
                        Phone = "Phone: 7776221"
                    };
                    Console.WriteLine("\nCreate customer: " + await CreateCustomer.CreateApiCustomer(customer));
                }
                else
                    Console.WriteLine("\nОшибка. Команда не распознана. Повторите ввод");
            }
        }
    }
}
