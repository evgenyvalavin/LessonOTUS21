﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace consoleRequestsAPI
{
    public static class GetCustomer
    {
        private static HttpClient Client = new HttpClient();
        public static async Task<string> GetApiCustomer(int id)
        {
            try
            {
                var response = await Client.GetAsync("http://localhost:55932/customer/" + id);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    return response.StatusCode.ToString();
                else
                    return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                return "Check if API running!";
            }
        }
    }
}
