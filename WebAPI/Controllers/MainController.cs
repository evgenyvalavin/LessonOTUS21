﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class MainController : ApiController
    {
        private static List<Customer> customers = new List<Customer>();

        [Route("~/customer/{id}")]
        public HttpResponseMessage Get(int id)
        {
            if (customers.Any(x => x.Id == id))
                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(customers.Where(x => x.Id == id).FirstOrDefault()));
            else
                return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [Route("~/customer")]
        public async Task<HttpResponseMessage> Post()
        {
            var customer = JsonConvert.DeserializeObject<Customer>(await Request.Content.ReadAsStringAsync());
            if (customers.Any(x => x.Id == customer.Id))
                return Request.CreateResponse(HttpStatusCode.Conflict);
            else
                customers.Add(customer);
            return Request.CreateResponse(HttpStatusCode.Created);
        }


    }
}
