﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static string _dataFileName;
        private static int _dataCount = 100_000;

        public static async void Main(string[] args)
        {
            if (!await TryValidateAndParseArgs(args))
                return;

            Console.WriteLine("Generating xml data...");

            var generator = GeneratorFactory.GetGenerator(args[0], _dataCount);
            generator.Generate();

            Console.WriteLine($"Generating xml data in {_dataFileName}...");
        }

        private static Task<bool> TryValidateAndParseArgs(string[] args)
        {
            return Task.Run(() =>
            {
                if (args != null && args.Length > 0)
                {
                    _dataFileName = Path.Combine(args[0], $"{args[0]}.xml");
                }
                else
                {
                    Console.WriteLine("Data file name without extension is required");
                    return false;
                }

                if (args.Length > 1)
                {
                    if (!int.TryParse(args[1], out _dataCount))
                    {
                        Console.WriteLine("Data must be integer");
                        return false;
                    }
                }
                return true;
            });
        }
    }
}