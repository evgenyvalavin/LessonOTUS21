﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class SaveData
    {
        public Task ThreadSaving(List<Customer> data, int threadCount)
        {
            return Task.Run(() =>
            {
                List<Thread> threadList = new List<Thread>();
                int forEach = data.Count / threadCount;
                for (int i = 0; i < threadCount; i++)
                {
                    var from = i * forEach + i;
                    var take = forEach;
                    if (threadCount == i + 1)
                        take = data.Count - from - take != 0 ? data.Count - from : take;
                    threadList.Add(new Thread(async () => await SomeWork(data, from, take)));
                    threadList[i].Start();
                }
                bool working = true;
                do
                {
                    working = !threadList.TrueForAll(x => x.ThreadState == ThreadState.Stopped);
                } while (working);
            });
        }

        public async Task ThreadPoolSaving(List<Customer> data, int threadPoolCount)
        {
            List<Task> taskList = new List<Task>();
            int forEach = data.Count / threadPoolCount;
            for (int i = 0; i < threadPoolCount; i++)
            {
                var from = i * forEach + i;
                var take = forEach;
                if (threadPoolCount == i + 1)
                    take = data.Count - from - take != 0 ? data.Count - from : take;
                taskList.Add(SomeWork(data, from, take));
            }
            await Task.WhenAll(taskList);
        }

        private async Task SomeWork(List<Customer> data, int from, int take)
        {
            Console.WriteLine($"For thread {Thread.CurrentThread.ManagedThreadId}: from " + from + " take " + take);
            foreach (var item in data.Skip(from).Take(take))
                await Task.Delay(5);
        }
    }
}
