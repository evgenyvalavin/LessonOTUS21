﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Otus.Teaching.Concurrency.Import.Loader.Parser
{
    public class MyPArser : IDataParser<List<Customer>>
    {
        private List<Customer> customers;

        public List<Customer> Parse(string path)
        {
            customers = new List<Customer>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlElement xRoot = xDoc.DocumentElement;
            XmlElement xmlElement = xRoot["Customers"];

            foreach (XmlNode xnode in xmlElement)
            {
                Customer customer = new Customer();
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    if (childnode.Name == "Id")
                        customer.Id = Convert.ToInt32(childnode.InnerText);
                    if (childnode.Name == "FullName")
                        customer.FullName = childnode.InnerText;
                    if (childnode.Name == "Email")
                        customer.Email = childnode.InnerText;
                    if (childnode.Name == "Phone")
                        customer.Phone = childnode.InnerText;
                }
                customers.Add(customer);
            }
            return customers;
        }

        private void ThreadParse(int startValue, int endValue, XmlElement xmlElement)
        {

        }
    }
}
