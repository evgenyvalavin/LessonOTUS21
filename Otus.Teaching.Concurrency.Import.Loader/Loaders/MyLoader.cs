﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Parser;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class MyLoader : IDataLoader
    {
        public Task<List<Customer>> LoadData(string path)
        {
            return Task.Run(() =>
            {
                MyPArser loader = new MyPArser();
                return loader.Parse(path);
            });
        }
    }
}
