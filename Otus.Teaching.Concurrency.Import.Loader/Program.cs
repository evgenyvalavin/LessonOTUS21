﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath;
        private const string createDataPath = @"\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
        public static List<Customer> data;

        static async void Main(string[] args)
        {
            var path = @"C:\Users\evgen\Downloads\Otus.Teaching.Concurrency.Import-master\Otus.Teaching.Concurrency.Import-master";
            ProcessStartInfo processStartInfo = new ProcessStartInfo()
            {
                FileName = path + createDataPath,
                Arguments = path + @"\DataFile.xml",
            };
            await Task.Run(() =>
            {
                using var p = Process.Start(processStartInfo);
                if (p != null)
                    p.WaitForExit();
            }).ConfigureAwait(false);
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var loader = new MyLoader();
            data = await loader.LoadData(path + @"\DataFile.xml").ConfigureAwait(false);

            var dataSaver = new SaveData();
            var watcher = new Stopwatch();
            watcher.Start();
            await dataSaver.ThreadSaving(data, 33).ConfigureAwait(false);
            Console.WriteLine($"Time taken by threads: {watcher.Elapsed}");

            watcher = new Stopwatch();
            watcher.Start();
            await dataSaver.ThreadPoolSaving(data, 33).ConfigureAwait(false);
            Console.WriteLine($"Time taken by ThreadPoolSaving: {watcher.Elapsed}");
        }

        static void GenerateCustomersDataFile()
        {

        }
    }
}